defmodule Lider do
	use Supervisor
	
	@moduledoc """
		Este módulo asume el rol del lider de una serie de trabajadores que implementan 
		el juego "siete y medio". 
	"""
	
	#Client API
	
	#Crea distintos trabajadores con una instancia del juego siete y medio.
	#Precondición: juegos debe ser una lista de átomos [:juego1,:juego2,..., juegoN].
	def iniciar_juegos(juegos) do
		Supervisor.start_link(__MODULE__, juegos)
	end
	
	#Supervisor API
	
	#Formatea la lista de átomos para poder llamar a Supervisor init
	#y crear los trabajadores.
	def init(juegos) do
		juegos = 
			Enum.map(
				juegos,
				fn nombre ->
					%{
						id: nombre,
						start: {Trabajador, :crear, [nombre]},
						shutdown: 5_000, #Por defecto
						type: :worker
					}
				end
			)	
		Supervisor.init(juegos, strategy: :one_for_one)	
	end
end
