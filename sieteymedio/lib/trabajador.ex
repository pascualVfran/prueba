defmodule Trabajador do
	use GenServer
	
	@moduledoc """
		Este módulo asume el rol del trabajador que maneja una instancia  
		del juego "siete y medio" ralizando las llamadas necesarias al módulo 
		que lo implementa. 
	"""
	
	#Leader API
	
	#Crea las instancias del juego 
	def crear(nombre_juego) do
		IO.puts(nombre_juego)
		GenServer.start_link(__MODULE__,[SieteYMedio.crear_partida()],name: {:global,nombre_juego})
	end
	
	#Client API
	
	def inscribirse(nombre_juego, jugador) do
		GenServer.cast({:global,nombre_juego},{:inscribir, jugador})
	end
	
	def ver_carta(nombre_juego, jugador) do
		GenServer.cast({:global,nombre_juego},{:ver, jugador})
	end
	
	def pedir_carta(nombre_juego, jugador) do
		GenServer.cast({:global,nombre_juego},{:pedir, jugador})
	end
	
	def plantarse(nombre_juego, jugador) do
		GenServer.cast({:global,nombre_juego},{:plantarse, jugador})
	end
	
	def resultado(nombre_juego) do
		GenServer.cast({:global,nombre_juego},{:resultado})
	end
	
	
	#GenServer API
	
	def init([partida]) do
      {:ok, partida}
    end
    
    def handle_cast({:inscribir, nombre_jugador},{:creacion,jugadores,baraja}) do
		{estado,nuevos_jugadores,baraja} = SieteYMedio.crear_jugador({:creacion,jugadores,baraja})
		
		#Obtenemos el último jugador
		IO.write "Eres el jugador :"
		Enum.take(nuevos_jugadores,-1)
			|> hd
			|> IO.puts
		
		{:noreply,{estado,nuevos_jugadores,baraja}}
	end
	def handle_cast({:inscribir, _},partida) do
		IO.puts("Lo sentimos. No es posible unirse a la partida.")
		{:noreply,partida}
	end
	
	def handle_cast({:ver,jugador},{{:jugando,jugador_actual},jugadores,baraja}) do
		SieteYMedio.ver_carta({{:jugando,jugador_actual},jugadores,baraja},jugador)
		{:noreply, {{:jugando,jugador_actual},jugadores,baraja}}
	end


	def handle_cast({:pedir,jugador},partida) do
		{:noreply, SieteYMedio.pedir_carta(partida, jugador)}
	end
	
	
	def handle_cast({:plantarse,jugador},partida) do
		{:noreply, SieteYMedio.plantarse(partida,jugador)}
	end	
	
	def handle_cast({:resultado},{:finalizada,puntuaciones,baraja}) do
		mostrar_puntuaciones(puntuaciones)
		{:noreply, {:finalizada,puntuaciones,baraja}}
	end
	
	defp mostrar_puntuaciones([]), do: :ok
	defp mostrar_puntuaciones([{ganador,puntuacion}|t]) do
		IO.puts ganador
		IO.puts puntuacion
		mostrar_puntuaciones(t)
	end
	
	
end
